import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from xgboost import XGBClassifier
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, confusion_matrix, classification_report
import seaborn as sns
from mlxtend.plotting import plot_confusion_matrix
from sklearn.model_selection import KFold
from sklearn.model_selection import cross_val_score
# from sklearn.ensemble import (
#     RandomForestClassifier,
#     ExtraTreesClassifier,
# )


#==========================DATA SELECTION AND LOADING==========================
df=pd.read_csv("UNSW_NB15.csv")
data=df.copy()
print(df.head())
print(df.info())
print(df.describe())

attack_cat = df['attack_cat']

df.drop(df.loc[df['attack_cat'] == 'Shellcode'].index, inplace=True)
print(set(df['attack_cat']))


#=============================DATA PRE-PROCESSING==============================
df[df['service']=='-']

df['service'].replace('-',np.nan,inplace=True)
print('-----------------------Before Pre-Processing--------------------------')
print()
print("Checking Missing Values:", df.isnull().sum())
print("Checking Missing Values:", df.isnull().sum().any())
print(df.shape)

print()
print('------------------------After Pre-Processing--------------------------')
df.dropna(inplace=True)
print("done")
print("Checking Missing Values:", df.isnull().sum())
print("Checking Missing Values:", df.isnull().sum().any())
print(df.shape)

df['attack_cat'].value_counts()

sns.countplot(x ='attack_cat', data = df)
plt.show()

plt.figure(figsize=(8,8))
plt.pie(df.attack_cat.value_counts(),labels=df.attack_cat.unique(),autopct='%0.2f%%')
plt.title('Pie chart distribution of multi-class labels')
plt.legend(loc='best')
plt.show()

#Label Encoding
from sklearn.preprocessing import LabelEncoder
le=LabelEncoder()
df.iloc[:,2]=le.fit_transform(df.iloc[:,2])
df.iloc[:,3]=le.fit_transform(df.iloc[:,3])
df.iloc[:,4]=le.fit_transform(df.iloc[:,4])
df.iloc[:,43]=le.fit_transform(df.iloc[:,43])

X = df.drop(columns=['attack_cat'],axis=1)
Y = df['attack_cat']

#=============================DATA SPLITTING===================================
x_train,x_test,y_train,y_test = train_test_split(X,Y,test_size=0.30, random_state=100)

print(x_train.shape)
print(x_test.shape)
print(y_train.shape)
print(y_test.shape)
print()
#==============================CLASSIFICATION==================================
#===========================ExtraTreesClassifier===============================
'''ExtraTree Classifier'''

print('ExtraTree Classifier')
print()
classifier = ExtraTreesClassifier()
classifier.fit(x_train,y_train)


kf = KFold(n_splits=3)
extra_tree =ExtraTreesClassifier(n_estimators=250,max_depth=7, bootstrap=True,n_jobs=-1)
extra_tree.fit(x_train, y_train)
et_pred = extra_tree.predict(x_test)
etc=(accuracy_score(y_test, et_pred)*100)
print("ETC accuracy is: ", etc,'%')
print()
print('Classification Report')
cr=classification_report(y_test, et_pred)
print(cr)
et_cm = confusion_matrix(y_test, et_pred)
print(et_cm)
print()
et_tn = et_cm[0][0]
et_fp = et_cm[0][1]
et_fn = et_cm[1][0]
et_tp = et_cm[1][1]
print('et_tn' + str(et_cm[0][0]))
print('et_fp' + str(et_cm[0][1]))
print('et_fn' + str(et_cm[1][0]))
print('et_tp' + str(et_cm[1][1]))
Total_TP_FP=et_cm[0][0]+et_cm[0][1]
Total_FN_TN=et_cm[1][0]+et_cm[1][1]
specificity = et_tn / (et_tn+et_fp)
et_specificity=format(specificity,'.3f')
print('ETC_specificity:',et_specificity)
print()

class_names = ['Analysis', 'Backdoor', 'DoS', 'Exploits', 'Fuzzers', 'Generic','Normal', 'Reconnaissance', 'Worms']
#Confusion Matrix Plot
plt.figure()
plot_confusion_matrix(conf_mat=et_cm, figsize=(15,15), show_normed = True);

plt.title( "Model confusion matrix")
plt.style.use("ggplot")
plt.show()
extra_tree_result = cross_val_score(extra_tree,x_train,y_train,cv=kf,n_jobs=-1)
print(" extra tree results:",extra_tree_result)
print("average of extra tree:",np.mean(extra_tree_result))

# et_pred = classifier.predict(x_test)
# etc=(accuracy_score(y_test, et_pred)*100)
# print("ETC accuracy is: ", etc,'%')
# print()
# print('Classification Report')
# cr=classification_report(y_test, et_pred)
# print(cr)
# et_cm = confusion_matrix(y_test, et_pred)
# print(et_cm)
# print()
# et_tn = et_cm[0][0]
# et_fp = et_cm[0][1]
# et_fn = et_cm[1][0]
# et_tp = et_cm[1][1]
# print('et_tn' + str(et_cm[0][0]))
# print('et_fp' + str(et_cm[0][1]))
# print('et_fn' + str(et_cm[1][0]))
# print('et_tp' + str(et_cm[1][1]))
# Total_TP_FP=et_cm[0][0]+et_cm[0][1]
# Total_FN_TN=et_cm[1][0]+et_cm[1][1]
# specificity = et_tn / (et_tn+et_fp)
# et_specificity=format(specificity,'.3f')
# print('ETC_specificity:',et_specificity)
# print()

# class_names = ['Analysis', 'Backdoor', 'DoS', 'Exploits', 'Fuzzers', 'Generic','Normal', 'Reconnaissance', 'Worms']
# #Confusion Matrix Plot
# plt.figure()
# plot_confusion_matrix(conf_mat=et_cm, figsize=(15,15), show_normed = True);

# plt.title( "Model confusion matrix")
# plt.style.use("ggplot")
# plt.show()

#=============================RandomForestClassifier===========================
'''RandomForestClassifier'''
print('RandomForestClassifier')

rf = RandomForestClassifier()
rf.fit(x_train,y_train)
rf_pred = rf.predict(x_test)
RF= accuracy_score(y_test,rf_pred)*100
print("RandomForestClassifier Accuracy:",RF,'%')

print("------Classification Report------")
print(classification_report(rf_pred,y_test))
print('\n')
print('Confusion_matrix')
rf_cm = confusion_matrix(y_test, rf_pred)
print(rf_cm)
print('\n')
tn = rf_cm[0][0]
fp = rf_cm[0][1]
fn = rf_cm[1][0]
tp = rf_cm[1][1]
Total_TP_FP=rf_cm[0][0]+rf_cm[0][1]
Total_FN_TN=rf_cm[1][0]+rf_cm[1][1]
specificity = tn / (tn+fp)
rf_specificity=format(specificity,'.3f')
print('RF_specificity:',rf_specificity)

#Confusion Matrix Plot
plt.figure()
plot_confusion_matrix(rf_cm,figsize=(15,15), class_names = ['Analysis', 'Backdoor', 'DoS', 'Exploits', 'Fuzzers', 'Generic', 'Normal', 'Reconnaissance', 'Worms'], show_normed = True)

plt.title( "Model confusion matrix")
plt.style.use("ggplot")
plt.show()

#===========================LEARNING CURVE GRAPHS============================
from yellowbrick.model_selection import learning_curve
try:
    print(learning_curve(extra_tree, x_train, y_train, cv=6, scoring='accuracy'))
except:
    print("Learning Curve Warning")

from yellowbrick.model_selection import learning_curve
try:
    print(learning_curve(rf, x_train, y_train, cv=6, scoring='accuracy'))
except:
    print("Learning Curve Warning")

#===========================PERFORMANCE COMPARISION============================
print("-----------------------PERFORMANCE ESTIMATION-------------------------")
vals=[etc, RF]
inds=range(len(vals))
labels=["ETC ","RF"]
fig,ax = plt.subplots()
rects = ax.bar(inds, vals)
ax.set_xticks([ind for ind in inds])
ax.set_xticklabels(labels)
plt.show()
#==============================================================================
